import java.io.*;
import java.net.*;

public class Cliente {
	public static void main(String args[]) throws Exception {
 
		BufferedReader entTeclado = new BufferedReader(new InputStreamReader(System.in));
 
		DatagramSocket socket = new DatagramSocket();
 
		String servidor = "localhost";
		int porta = 9876;
 
		InetAddress IPAddress = InetAddress.getByName(servidor);
 
		byte[] DadosEnviar = new byte[100];
		byte[] DadosReceber = new byte[256];
 
		System.out.println("Digite o texto a ser enviado ao servidor: ");
		String stringEnviar = entTeclado.readLine();
		DadosEnviar = stringEnviar.getBytes();
		DatagramPacket pacoteEnviar = new DatagramPacket(DadosEnviar,
				DadosEnviar.length, IPAddress, porta);
 
		System.out.println("Enviando pacote de dados para " + servidor + ":" + porta);
		socket.send(pacoteEnviar);
 
		//recebendo dados do servidor
		DatagramPacket pacoteReceber = new DatagramPacket(DadosReceber,
				DadosReceber.length);
 
		socket.receive(pacoteReceber);
		System.out.println("Pacote recebido...\n");
 
		String stringModificada = new String(pacoteReceber.getData());
 
		System.out.println("Texto recebido do servidor: \n" + stringModificada);
		socket.close();
	

	}
}